import math
import  os
import  sys
from math import*


def main() :
	input_mesh = raw_input("Mesh : ")	
	input_theta_degree = raw_input("Theta (in degrees): ")
	Ecis=0.05
	os.system("rm -f periodic_plastic.inp")

	fin = open("periodic_plastic.inp.tmpl" , 'r')
        lines = fin.readlines()
        input_theta = float(input_theta_degree) * 3.1415927 /180.
	E11 = -Ecis*sin(2.*float(input_theta))
	E22 = Ecis*sin(2.*float(input_theta))
	E12 = Ecis*(cos(float(input_theta))*cos(float(input_theta))-sin(float(input_theta))*sin(float(input_theta)))
		

	i=0
	for line in lines :
		if "VAL_E11" in line :
                               lines[i] = line.replace("VAL_E11",str(E11))	
		else :
			if "VAL_E22" in line :
                               lines[i] = line.replace("VAL_E22",str(E22))
			else :
				if "VAL_E12" in line :
                               		lines[i] = line.replace("VAL_E12",str(E12))
				else :
					if "MESH" in line :
						lines[i] = line.replace("MESH",str(input_mesh))
		i=i+1

	fout = open("periodic_plastic.inp",'w')
        fout.writelines(lines)
main()

