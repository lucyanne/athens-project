#!/bin/bash

for R in 1 2 3
do 
    for L in 100 200
    do
        input="calcul_Vv30_R${R}_L${L}.geof"
        output="periodic_R${R}_L${L}.post
        sed -i "s/calcul.*\.geof/$input/gi" <periodic.inp >periodicA.inp

        Zrun periodicA.inp 
        Zrun -pp periodicA.inp 
        cat periodicA.post >$output
    done
done



